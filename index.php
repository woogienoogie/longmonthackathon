<?php
$day = $_POST['input_day'];
$side = $_POST['input_location'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en">
<head>
<title>Longmont Trash/Recycle Scheduling</title>
<link rel="stylesheet" href="style/main.css" />
<link href="lightbox/css/lightbox.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.23.custom.min.js"></script>
<script src="lightbox/js/lightbox.js"></script>
<script type="text/javascript">
// Enter submit
$(document).ready(function()
{
	$(document).keypress(function(event)
	{
		if ( event.which == 13 )
		{
			falseSubmit();
		}
	});
});

// Set the variables, so all functions can use it
var stopSubmit = "false";
var email = "";
var day = "";
var side = "";

function validateFields()
{
	// Check email
	email = document.getElementById('email').value;
	if (email == '')
	{
		document.getElementById('emailWarn').setAttribute("style","display:block; color: red;");
		document.getElementById('emailWarn').innerHTML = "You must enter your email.";
		stopSubmit = "true";
	}
	//Validate email
	email = document.getElementById('email').value;
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(email))
	{
	document.getElementById('emailWarn').setAttribute("style","display:block; color: red;");
	document.getElementById('emailWarn').innerHTML = "You must enter your email.";
    stopSubmit = "true";
	}
    else
	{
	document.getElementById('emailWarn').setAttribute("style","display:none;");
	document.getElementById('emailWarn').innerHTML = "";
	}
	
	// Check day
	var dayChecked = "false";
	
	if (document.getElementById('Monday').checked)
	{
		dayChecked = "true";
		document.getElementById('dayWarn').setAttribute("style","display:none;");
		document.getElementById('dayWarn').innerHTML = "";
		day = "Monday";
	}
	if (document.getElementById('Tuesday').checked)
	{
		dayChecked = "true";
		document.getElementById('dayWarn').setAttribute("style","display:none;");
		document.getElementById('dayWarn').innerHTML = "";
		day = "Tuesday";
	}
	if (document.getElementById('Wednesday').checked)
	{
		dayChecked = "true";
		document.getElementById('dayWarn').setAttribute("style","display:none;");
		document.getElementById('dayWarn').innerHTML = "";
		day = "Wednesday";
	}
	if (document.getElementById('Thursday').checked)
	{
		dayChecked = "true";
		document.getElementById('dayWarn').setAttribute("style","display:none;");
		document.getElementById('dayWarn').innerHTML = "";
		day = "Thursday";
	}
	if (document.getElementById('Friday').checked)
	{
		dayChecked = "true";
		document.getElementById('dayWarn').setAttribute("style","display:none;");
		document.getElementById('dayWarn').innerHTML = "";
		day = "Friday";
	}
	if (dayChecked !== "true")
	{
		document.getElementById('dayWarn').setAttribute("style","display:block; color: red;");
		document.getElementById("dayWarn").innerHTML = "You must select a day.";
		stopSubmit = "true";
	}
	
	// Check side
	var sideChecked = "false";
	
	if (document.getElementById('West').checked)
	{
		sideChecked = "true";
		document.getElementById('sideWarn').setAttribute("style","display:none;");
		document.getElementById('sideWarn').innerHTML = "";
		side = "West";
	}
	if (document.getElementById('East').checked)
	{
		sideChecked = "true";
		document.getElementById('sideWarn').setAttribute("style","display:none;");
		document.getElementById('sideWarn').innerHTML = "";
		side = "East";
	}
	if (sideChecked !== "true")
	{
		document.getElementById('sideWarn').setAttribute("style","display:block; color: red;");
		document.getElementById("sideWarn").innerHTML = "You must select a side.";
		stopSubmit = "true";
	}
	
	// Check if submission needs to be stopped
	if (stopSubmit == "true")
	{
		alert("Please fix the errors before submitting.");
	}
}

function falseSubmit()
{	
	validateFields();
	
	if(stopSubmit !== "true")
	{
		window.location = "process.php?email=" + email + "&day=" + day + "&side=" + side + "";
	}
	
	// Set stopSubmit back to false to force the script to revalidate
	stopSubmit = "false";
}
</script>
</head>
<body>
<div id="light" class="white_content"></div>
<div id="fade" class="black_overlay"></div>
<div class="wrapper">
<div id="emailWarn" style="display:none;"></div>
<h3>
Email address:
<br />
<input type="text" size="30" maxlength="100" name="email" id="email"/>
</h3>
<div id="dayWarn" style="display:none;"></div>
<h3>Pickup Day</h3>
<div class="container"><input type="radio" name="day" value="Monday" id="Monday" <?php if($day == "Monday"){ echo "checked "; } ?>/> Monday</div>
<div class="container"><input type="radio" name="day" value="Tuesday" id="Tuesday" <?php if($day == "Tuesday"){ echo "checked "; } ?>/> Tuesday</div>
<div class="container"><input type="radio" name="day" value="Wednesday" id="Wednesday" <?php if($day == "Wednesday"){ echo "checked "; } ?>/> Wednesday</div>
<div class="container"><input type="radio" name="day" value="Thursday" id="Thursday" <?php if($day == "Thursday"){ echo "checked "; } ?>/> Thursday</div>
<div class="container"><input type="radio" name="day" value="Friday" id="Friday" <?php if($day == "Friday"){ echo "checked "; } ?>/> Friday</div>
<div id="sideWarn" style="display:none;"></div>
<h3>Recylce Side</h3>
<div class="container"><input type="radio" name="side" value="West" id="West" <?php if($side == "West"){ echo "checked "; } ?>/> West</div>
<div class="container"><input type="radio" name="side" value="East" id="East" <?php if($side == "East"){ echo "checked "; } ?>/> East</div>
<h3>Don't know your day or side? <a href="map/map.html">Click here</a>.</h3>
<br />
<div class="containerbottom"><button id="SubmitButton" onclick=falseSubmit()>Submit</button></div>
</div>
</body>
</html>