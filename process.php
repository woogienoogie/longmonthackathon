<?php
require 'wrapper/src/Ctct/autoload.php';

$email = $_GET['email'];
$day = $_GET['day'];
$side = $_GET['side'];

if($side == "East")
{
	if($day == "Monday")
	{
		$list = "1649115972";
	}
	elseif($day == "Tuesday")
	{
		$list = "1808713583";
	}
	elseif($day == "Wednesday")
	{
		$list = "2090478106";
	}
	elseif($day == "Thursday")
	{
		$list = "1438287806";
	}
	elseif($day == "Friday")
	{
		$list = "1079744562";
	}
}
elseif($side == "West")
{
	if($day == "Monday")
	{
		$list = "1578953118";
	}
	elseif($day == "Tuesday")
	{
		$list = "1403666412";
	}
	elseif($day == "Wednesday")
	{
		$list = "1816510619";
	}
	elseif($day == "Thursday")
	{
		$list = "1611914672";
	}
	elseif($day == "Friday")
	{
		$list = "1636170226";
	}
}

use Ctct\ConstantContact;
use Ctct\Components\Contacts\Contact;
use Ctct\Components\Contacts\ContactList;
use Ctct\Components\Contacts\EmailAddress;
use Ctct\Exceptions\CtctException;

define("APIKEY", "74zt3gs2jtwjqzdsthpr59r6");
define("ACCESS_TOKEN", "4c8e15c1-03e8-417a-8b5e-0899b809101d");

$cc = new ConstantContact(APIKEY);

// attempt to fetch lists in the account, catching any exceptions and printing the errors to screen
try
{
    $lists = $cc->getLists(ACCESS_TOKEN);
}
catch (CtctException $ex)
{
    foreach ($ex->getErrors() as $error)
	{
        print_r($error);
    }     
}

// check if the form was submitted
if (isset($_GET['email']) && strlen($_GET['email']) > 1) {
    $action = "Getting Contact By Email Address";
    try {
        // check to see if a contact with the email addess already exists in the account
        $response = $cc->getContactByEmail(ACCESS_TOKEN, $_GET['email']);

        // create a new contact if one does not exist
        if (empty($response->results)) {
            $action = "Creating Contact";

            $contact = new Contact();
            $contact->addEmail($email);
            $contact->addList($list);
            $returnContact = $cc->addContact(ACCESS_TOKEN, $contact); 

        // update the existing contact if address already existed
        } else {            
            $action = "Updating Contact";

            $contact = $response->results[0];
            $contact->addList($list);
            $returnContact = $cc->updateContact(ACCESS_TOKEN, $contact);  
        }
        
    // catch any exceptions thrown during the process and print the errors to screen
    } catch (CtctException $ex) {
        echo '<span class="label label-important">Error '.$action.'</span>';
        echo '<div class="container alert-error"><pre class="failure-pre">';
        print_r($ex->getErrors()); 
        echo '</pre></div>';
        die();
    }
} 

if (isset($returnContact)) {
        echo "Success!";
    }
?>